<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Inicio');
})->name('Inicio');

Route::get('SobreMi', function () {
    return view('SobreMi');
});

Route::get('Portafolio', function () {
    return view('Portafolio');
});

Route::get('Contacto', function () {
    return view('Contacto');
});

Route::get('Login', function(){
	return view('Login');
});

Route::post('messages', function(){

	//Enviar un correo

	$data = request()->all();
	Mail::send("emails/message", $data, function($message) use ($data){

		$message->from($data['email'], $data['name'])
		->to('daninacho99@gmail.com', 'Daniel')
		->subject($data['subject']);

	});
	//Responder al usuario

	return back();

})->name('messages');
Auth::routes();
Route::get('/home', 'HomeController@index');



Route::resource('image','imageController');
Route::get('image/destroy/{id}',['as'=>'image/destroy','uses'=>'imageController@destroy']);
Route::get('image/search',['as'=>'image/search','uses'=>'imageController@search']);
