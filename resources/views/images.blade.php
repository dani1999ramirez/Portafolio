@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		{!! Form::open(['route' => 'image.store', 'method'=> 'post', 'novalidate']) !!}

			{!! Form::label('id') !!}

			{!! Form::number('id',null,['class'=>'form-control'])!!}
			
			{!! Form::label('Seleccion') !!}

			{!! Form::file('images',null,['class'=>'form-control'])!!}	
			{!! Form::submit('Enviar',['class'=>'btn-btn-primary'])!!}
		{!! Form::close() !!}
	</div>
</div>
@endsection