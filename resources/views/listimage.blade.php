@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		{!!Form::open(['route'=>'image/search','method'=>'post','novalidate','class'=>'form-inline'])!!}
		<div class="form-group">
			<label>Id</label>
			<input type="number" name="name" class="form-control">
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary">Buscar</button>
			<a href="{{ route('image.index')}}" class="btn btn-primary">Todo</a>
			<a href="{{ route('image.create')}}" class="btn btn-primary">Crear</a>
		</div>
		{!!Form::close()!!}
	</div>
	<div class="row">
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>id</th>
					<th>Foto</th>
				</tr>
			</thead>
			<tbody>
				@foreach($images as $image)
				<tr>
					<td>{{$image->id}}</td>
					<td>{{$image->images}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
</div>
@endsection