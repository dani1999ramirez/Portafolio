@extends('layout')

@section('content')
	<!-- 
        ================================================== 
            Global Page Section Start
        ================================================== -->
        <section class="global-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <h2>Portafolio</h2>
                        </div>
                    </div>
                </div>
            </div>   
        </section><!--/#Page header-->


         <section class="company-description">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                        <img src="images/RedSocial.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Pagina Red social</h3>
                            <p  class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration="500ms">
                                
                            </p>
                            <h3  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                                Diseño y programación: hecho por Daniel Ramírez en maquetación Html5, CSS y Jquery. 
                            </h3>

                            <h3  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                               Realizado: de Febrero a Marzo de 2017. 
                            </h3>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </br>
        <section class="company-description">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="block">
                            <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Pagina Login</h3>
                            
                            <h3  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                                Diseño y programación: hecho por Daniel Ramírez en maquetación Html5 y CSS. 
                            </h3>

                            <h3  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                               Realizado:  Febrero y marzo de 2017.
                            </h3>
                            
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                        <img src="images/LoginU.jpg" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </section>

        <section class="company-description">
            <div class="container">
                <div class="row">
                <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                        <img src="images/ProyectoVS.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Proyecto de calzado</h3>
                            
                            <h3  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                                Diseño y programación: hecho por Daniel Ramírez y Brayan Triana (amigo) en Visual Studio. Este proyecto se hizo durante el técnico.</h3> 
                                <h3  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">Un proyecto de un almacen de calzado donde requeria un sistema de información para la gestion de inventario, ventas y pagos del almacen.
                                </h3>

                            <h3  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                               Realizado:  2015 - 2016.
                            </h3>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>


@endsection