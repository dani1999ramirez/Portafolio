@extends('layoutM')

@section('content')
	 <!--
        ==================================================
        Slider Section Start
        ================================================== -->
        <section id="hero-area" class="hero-area" >
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="block wow fadeInUp" data-wow-delay=".3s">
                            
                            <!-- Slider -->
                            <section class="cd-intro">
                                <h1 class="wow fadeInUp animated cd-headline slide" data-wow-delay=".4s" >
                                <span>Hola, mi nombre es Daniel Ramírez</span><br>
                                <span class="cd-words-wrapper">
                                    <b class="is-visible">Análisis y</b>
                                    <b>Desarrollo de</b>
                                    <b>Sistemas de información</b>
                                </span>
                                </h1>
                                </section> <!-- cd-intro -->
                                <!-- /.slider -->
                                <h2 class="wow fadeInUp animated" data-wow-delay=".6s" >
                                    Siendo técnico en Programación de software y 
                                    ahora estudiando tecnólogo en <br>
                                    ADSI
                                </h2>                                
                            </div>
                        </div>
                    </div>
                </div>
            </section><!--/#main-slider-->

             <!--
            ==================================================
            Slider Section Start
            ================================================== -->
            <section id="about" class="about">
                <div class="container">
                    <div class="section-heading">
                        <div class="col-md-6 col-sm-6">
                                <h2 class="title wow fadeInDown" data-wow-delay=".3s" style="margin-top: 150px;">
                                ABOUT ME
                                </h2>
                                <p>
                                    Hola, mi nombre es José Daniel Ramírez Ramírez, soy técnico en Programación de software por el Sena (2015-2016), estoy estudiando Análisis y Desarrollo de Sistemas de Información en el Sena.  
                                </p>
                                <a class="btn-lines dark light wow fadeInUp animated smooth-scroll btn btn-default btn-green" data-wow-delay=".9s" href="SobreMi">Ver más...</a>
                            </div>
                            
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="block wow fadeInRight" data-wow-delay=".3s" data-wow-duration="500ms">
                                <img src="images/Meme.jpeg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section> <!-- /#about -->

             <!--
            ==================================================
            Portfolio Section Start
            ================================================== -->
            <section id="prtafolio" class="prtafolio">
                <div class="container">
                    <div class="section-heading">
                        <h1 class="title wow fadeInDown" data-wow-delay=".3s">PRTAFOLIOS</h1>
                        <p class="wow fadeInDown" data-wow-delay=".5s">
                            Veras todos mis trabajos solo hechos por mi. Miralos sin compromiso.
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                                <div class="img-wrapper">
                                    <img src="images/RedSocial.jpg" class="img-responsive" alt="this is a title" >
                                    <div class="overlay">
                                        <div class="buttons">
                                            <a rel="gallery" class="fancybox" href="images/RedSocial.jpg">Demo</a>
                                            <a target="_blank" href="single-portfolio.html">Ver más...</a>
                                        </div>
                                    </div>
                                </div>
                                <figcaption>
                                <h4>
                                <a href="#">
                                    Dew Drop
                                </a>
                                </h4>
                                <p>
                                    Redesigne UI Concept
                                </p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <figure class="wow fadeInLeft animated" data-wow-duration="500ms" data-wow-delay="300ms">
                                <div class="img-wrapper">
                                    <img src="images/LoginU.jpg" class="img-responsive" alt="this is a title" >
                                    <div class="overlay">
                                        <div class="buttons">
                                            <a rel="gallery" class="fancybox" href="images/LoginU.jpg">Demo</a>
                                            <a target="_blank" href="Portafolio">Ver más...</a>
                                        </div>
                                    </div>
                                </div>
                                <figcaption>
                                <h4>
                                <a href="#">
                                    Ejemplo
                                </a>
                                </h4>
                                <p>
                                    Lorem ipsum dolor sit.
                                </p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <figure class="wow fadeInLeft animated" data-wow-duration="500ms" data-wow-delay="300ms">
                                <div class="img-wrapper">
                                    <img src="images/ProyectoVS.jpg" class="img-responsive" alt="" >
                                    <div class="overlay">
                                        <div class="buttons">
                                            <a rel="gallery" class="fancybox" href="images/ProyectoVS.jpg">Demo</a>
                                            <a target="_blank" href="Portafolio">Ver más...</a>
                                        </div>
                                    </div>
                                </div>
                                <figcaption>
                                <h4>
                                <a href="#">
                                    Table Design
                                </a>
                                </h4>
                                <p>
                                    Lorem ipsum dolor sit amet.
                                </p>
                                </figcaption>
                            </figure>
                        </div><br>
                        <a class="btn-lines dark light wow fadeInUp animated smooth-scroll btn btn-default btn-green" data-wow-delay=".9s" href="Portafolio" style="margin-left: 530px; font-size: 18px;" >Ver más...</a>
                    </div>
                </div>
            </section> <!-- #works -->

            <!--
            ==================================================
            Portfolio Section Start
            ================================================== -->
            <section id="feature" class="feature">
                <div class="container">
                    <div class="section-heading">
                        <h1 class="title wow fadeInDown" data-wow-delay=".3s">Contacto</h1>
                        <h3 class="wow fadeInDown" data-wow-delay=".5s">
                            Contáctame tambien en mis redes sociales. 
                        </h3>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xs-12">
                            <div class="media wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
                                <div class="media-left">
                                <div class="media-body">
                                    <h4 class="media-heading" style="margin-left: 13px;">FACEBOOK</h4>
                                </div>
                                    <div class="icon">
                                        <i class="ion-ios-flask-outline"></i>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-xs-12">
                            <div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="600ms">
                                <div class="media-left">
                                <div class="media-body">
                                    <h4 class="media-heading" style="margin-left: 18px;"">TWITTER</h4>   
                                </div>
                                    <div class="icon">
                                        <i class="ion-ios-lightbulb-outline"></i> 
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-xs-12">
                            <div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="900ms">
                                <div class="media-left">
                                <div class="media-body">
                                    <h4 class="media-heading">INSTAGRAM</h4>
                                </div>
                                    <div class="icon">
                                        <i class="ion-ios-lightbulb-outline"></i> 
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <a class="btn-lines dark light wow fadeInUp animated smooth-scroll btn btn-default btn-green" data-wow-delay=".9s" href="contacto" style="margin-left: 540px; font-size: 18px;" >Ver más...</a>
                    </div>
                </div>
            </section> <!-- /#feature -->

            <span class="ir-arriba icon-arrow-with-circle-up"></span>


@endsection
 